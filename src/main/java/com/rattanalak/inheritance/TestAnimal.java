/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rattanalak.inheritance;

/**
 *
 * @author Rattanalak
 */
public class TestAnimal {

    public static void main(String[] args) {
        Animal animal = new Animal("Ani", "White", 0);
        animal.speak();
        animal.walk();
        NewLine();

        Dog dang = new Dog("Dang", "Black&White");
        dang.speak();
        dang.walk();
        NewLine();

        Cat zero = new Cat("Zero", "Orange");
        zero.speak();
        zero.walk();
        NewLine();

        Duck zom = new Duck("Zom", "Orange");
        zom.speak();
        zom.walk();
        zom.fly();
        NewLine();

        Dog To = new Dog("To", "Brown");
        To.speak();
        To.walk();
        NewLine();

        Dog mome = new Dog("Mome", "Black&White");
        mome.speak();
        mome.walk();
        NewLine();

        Dog bat = new Dog("Bat", "Black&White");
        bat.speak();
        bat.walk();
        NewLine();

        Duck gabGab = new Duck("GabGab", "Orange");
        gabGab.speak();
        gabGab.walk();
        gabGab.fly();
        NewLine();

        Animal[] animals = {dang, zero, zom, To, mome, bat, gabGab, animal};
        for (int i = 0; i < animals.length; i++) {
            if (animals[i] instanceof Dog) {
                System.out.println(animals[i].name + "is Animal: " + (animals[i] instanceof Animal));
                System.out.println(animals[i].name + "is Animal: " + (animals[i] instanceof Dog));
                System.out.println(animals[i].name + "is Animal: " + (animals[i] instanceof Object));
            } else if (animals[i] instanceof Cat) {
                System.out.println(animals[i].name + "is Animal: " + (animals[i] instanceof Animal));
                System.out.println(animals[i].name + "is Animal: " + (animals[i] instanceof Cat));
                System.out.println(animals[i].name + "is Animal: " + (animals[i] instanceof Object));
            } else if (animals[i] instanceof Duck) {
                System.out.println(animals[i].name + "is Animal: " + (animals[i] instanceof Animal));
                System.out.println(animals[i].name + "is Animal: " + (animals[i] instanceof Duck));
                System.out.println(animals[i].name + "is Animal: " + (animals[i] instanceof Object));
            } else if (animals[i] instanceof Animal) {
                System.out.println(animals[i].name + "is Animal: " + (animals[i] instanceof Animal));
                System.out.println(animals[i].name + "is Animal: " + (animals[i] instanceof Dog));
                System.out.println(animals[i].name + "is Animal: " + (animals[i] instanceof Cat));
                System.out.println(animals[i].name + "is Animal: " + (animals[i] instanceof Duck));
                System.out.println(animals[i].name + "is Animal: " + (animals[i] instanceof Object));
            }
            NewLine();
        }

        Animal[] animals1 = {dang, zero, zom, To, mome, bat, gabGab};
        for (int i = 0; i < animals1.length; i++) {
            animals1[i].speak();

            if (animals1[i] instanceof Duck) {
                Duck duck = (Duck) animals1[i];
                duck.fly();
            }
            animals1[i].walk();
            NewLine();
        }
    }

    private static void NewLine() {
        System.out.println("-----------------------------");
    }
}
